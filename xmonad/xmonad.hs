import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageDocks
import XMonad.Util.NamedWindows
import XMonad.Util.Run
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Layout.Spacing
import XMonad.Prompt
import XMonad.Prompt.Shell
import System.IO
import XMonad.Actions.SimpleDate
import XMonad.Actions.WindowBringer
import XMonad.Util.SpawnOnce


import qualified XMonad.StackSet as W

myStartupHook :: X ()
myStartupHook = do
	spawnOnce "nitrogen --set-scaled --random ~/wallpaper/ &"
	setWMName "LG3D"
	

main :: IO ()
main = do
	
    xmproc <- spawnPipe "xmobar /home/rabbie/.config/xmobar/xmobarrc"


    xmonad $ def
        { manageHook = manageDocks <+> manageHook def
        , layoutHook = avoidStruts $ spacingRaw True (Border 0 4 4 4) True (Border 4 4 4 4) True $layoutHook def
        , startupHook = myStartupHook
        , terminal = "terminator"
        , handleEventHook    = handleEventHook def <+> docksEventHook
	    , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
        , modMask = mod4Mask
        , focusedBorderColor = "#49FF00"
        } `additionalKeys`
        [ ((mod4Mask, xK_p), spawn "dmenu_run -c -l 10 -p run: ")
	    , ((controlMask .|. mod1Mask, xK_l), spawn "slock")

        , ((mod4Mask .|. shiftMask, xK_q), spawn "poweroff")
	    , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s -q 100 /home/rabbie/pictures/%Y-%m-%d-%H:%M:%S.png")
	    , ((0, xK_Print), spawn "scrot -q 100 /home/rabbie/pictures/%Y-%m-%d-%H:%M:%S.png")
        , ((mod4Mask, xK_d), date)
        , ((mod4Mask .|. shiftMask, xK_r), spawn "reboot")
        , ((mod4Mask, xK_b), spawn "notify-send  'Firefox' 'Opening firefox'; firefox")
        , ((mod4Mask .|. shiftMask, xK_t), spawn "stalonetray -p")
        , ((mod4Mask .|. shiftMask, xK_n), spawn "st newsboat")
        ]    
